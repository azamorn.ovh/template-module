import { Request, Response, Router } from 'express';
import { Module } from '../module';
import { AzamornOVH } from '../../src/main';
import path = require('path');
import fs = require('fs');

export class TemplateModule extends Module {
    app: AzamornOVH;
    template_regex: RegExp = /<~TEMPLATE:(\S+)~>/gm;
    constructor(app: AzamornOVH) {
        super("Template");
        this.app = app;
        return this;
    }
    templateReplacements(html): string {
        let matches;
        while ((matches = this.template_regex.exec(html)) !== null) {
            if (matches.index === this.template_regex.lastIndex) { this.template_regex.lastIndex++; }
            let template: Buffer | undefined = this.app.cache.get(`/template[${matches[1]}]`);
            if (template != undefined) {
                html = html.replace(matches[0], template.toString());
            }
            else {
                html = html.replace(matches[0], '');
            }
        }
        return html;
    }
    loadTemplateType(templateType, folderPath): Promise<Array<Promise<void>>> {
        return new Promise((resolve) => {
            let promises = [];
            fs.readdir(folderPath, {
                withFileTypes: true
            }, (err, files) => {
                for (let i = 0, n = files.length; i < n; i++) {
                    let ext: string | null = files[i].name.indexOf('.') != -1 ? files[i].name.split('.').pop() : null;
                    if (ext != null) {
                        if (ext.toLowerCase() == 'html') {
                            promises.push(new Promise((loaded) => {
                                fs.readFile(path.join(folderPath, files[i].name), (err, data) => {
                                    let templateName = files[i].name.split('.').shift();
                                    if (templateType == 'page') {
                                        let html = this.templateReplacements(data.toString());
                                        this.app.cache.set(`/${templateType}[${templateName}]`, Buffer.from(html, 'utf-8'));
                                    }
                                    else {
                                        this.app.cache.set(`/${templateType}[${templateName}]`, data);
                                    }
                                    loaded();
                                });
                            }));
                        }
                    }
                }
                resolve(promises);
            });
        })
    }
    init(): Promise<void> {
        return new Promise((init_finished) => {
            var web_router = Router(), api_router = Router();
            api_router.get('/templates/:template_slug', (req: Request, res: Response) => {
                let { template_slug } = req.params,
                    template: Buffer | undefined = this.app.cache.get(`/template[${template_slug}]`);
                if (template != undefined) {
                    res.status(200).send(template.toString());
                }
                else {
                    res.sendStatus(404);
                }
            });
            web_router.get('/', (req: Request, res: Response) => {
                let page: Buffer | undefined = this.app.cache.get(`/page[index]`);
                if (page != undefined) {
                    res.status(200).send(page.toString());
                }
            });
            web_router.get('/:page_slug', (req: Request, res: Response) => {
                let { page_slug } = req.params,
                    page: Buffer | undefined = this.app.cache.get(`/page[${page_slug}]`);
                if (page != undefined) {
                    res.status(200).send(page.toString());
                }
                else {
                    res.sendStatus(404);
                }
            });
            this.loadTemplateType('template', 'templates').then((template_promises) => {
                Promise.all(template_promises).then((templates) => {
                    this.loadTemplateType('page', 'pages').then((page_promises) => {
                        Promise.all(page_promises).then((pages) => {
                            this.app.web.use(web_router);
                            this.app.api.use(api_router);
                            init_finished();
                        })
                    })
                })
            })
        });
    }
}